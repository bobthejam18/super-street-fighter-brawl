﻿#include "window.h"
#include "game.h"

// Constructors
Window::Window(Game &game) : GAME(game)
{
    qDebug() << "Creation d'un objet Window";
    srand (time(NULL));
}

// Destructor
Window::~Window()
{
    qDebug() << "Suppression d'un objet Window";
    delAllGame_elements();
}

void Window::init_game()
{
    selected_arena = rand() % 2;
    delAllGame_elements();
    addAllGame_elements();
}

// Methods
void Window::update_game()
{
    for(Entity* elem : game_elements) { elem->update(); }
    for(Entity* elem : to_be_added) { addGame_element(elem); }
    to_be_added.clear();
    for(Entity* elem : to_be_deleted) { delGame_element(elem); }
    to_be_deleted.clear();
}

void Window::repaint_game(QPainter &painter)
{
    for(Entity* elem : game_elements) { elem->repaint(painter); }
}

// Getters and Setters
Game &Window::getGAME() const
{
    return GAME;
}

QVector<Entity *> Window::getGame_elements() const
{
    return game_elements;
}

void Window::addGame_element(Entity * entity)
{
    game_elements.append(entity);
}

void Window::delGame_element(Entity * entity)
{
    delete game_elements.takeAt(game_elements.indexOf(entity));
}

void Window::addAllGame_elements()
{
    QSize plateform_size;
    QVector<QPoint> player_spawns;
    QSize player_size = QSize(GAME.size().width()*165/1920,GAME.size().height()*165/1080);
    QVector<QString> player_skins = {"DBZ_Character"+QString::number(rand() % 9 + 1), "DBZ_Character"+QString::number(rand() % 9 + 1)};
    QVector<double> player_movement_stats = {double(GAME.size().width())*665/1920, double(GAME.size().height())*2000/1080, double(GAME.size().height())*90/1080};
    QVector<double> player_kick_stats = {double(GAME.size().width())*100/1920, 20, 500};
    QVector<double> player_projectile_stats = {double(GAME.size().width())*1850/1920, 40, 1000};
    QVector<double> player_shield_stats = {50, 250};
    switch (selected_arena) {
    case 0:
        plateform_size = QSize(400,80);
        player_spawns = {QPoint(GAME.size().width()/8-(player_size.width()/2),GAME.size().height()-(GAME.size().height()/2)-(player_size.height()/2)), QPoint(GAME.size().width()-(GAME.size().width()/8)-(player_size.width()/2),GAME.size().height()-(GAME.size().height()/2)-(player_size.height()/2))};
        addGame_element(new Background(QPixmap(":/Background_Sprites/Sprites/Backgrounds/glacial_mountains_fullcolor_preview.png"),*this));
        addGame_element(new Platform(QPoint(GAME.size().width()*25/100-(plateform_size.width()+100)/2, GAME.size().height()*80/100-plateform_size.height()),QSize(plateform_size.width()+100,plateform_size.height()),*this));
        addGame_element(new Platform(QPoint(GAME.size().width()*75/100-(plateform_size.width()+100)/2, GAME.size().height()*80/100-plateform_size.height()),QSize(plateform_size.width()+100,plateform_size.height()),*this));
        addGame_element(new Platform(QPoint(GAME.size().width()/2-plateform_size.width()/2, GAME.size().height()/2-plateform_size.height()/2),plateform_size,*this));
        addGame_element(new Projectile_Caster(player_spawns[0],player_size,"Fighter 1", player_skins[0], 100, player_movement_stats[0], player_movement_stats[1], player_movement_stats[2], true, Qt::Key::Key_Q, Qt::Key::Key_D, Qt::Key::Key_Z, *this));
        addGame_element(new Shield_Wielder(player_spawns[1],player_size,"Fighter 2", player_skins[1], 100, player_movement_stats[0], player_movement_stats[1], player_movement_stats[2], false, Qt::Key::Key_Left, Qt::Key::Key_Right, Qt::Key::Key_Up, *this));
        break;
    case 1:
        plateform_size = QSize(1250,80);
        player_spawns = {QPoint(GAME.size().width()/6-(player_size.width()/2),GAME.size().height()-(GAME.size().height()/2)-(player_size.height()/2)), QPoint(GAME.size().width()-(GAME.size().width()/6)-(player_size.width()/2),GAME.size().height()-(GAME.size().height()/2)-(player_size.height()/2))};
        addGame_element(new Background(QPixmap(":/Background_Sprites/Sprites/Backgrounds/glacial_mountains_fullcolor_preview.png"),*this));
        addGame_element(new Platform(QPoint(GAME.size().width()/2-plateform_size.width()/2, GAME.size().height()*80/100-plateform_size.height()),plateform_size,*this));
        addGame_element(new Projectile_Caster(player_spawns[0],player_size,"Fighter 1", player_skins[0], 100, player_movement_stats[0], player_movement_stats[1], player_movement_stats[2], true, Qt::Key::Key_Q, Qt::Key::Key_D, Qt::Key::Key_Z, *this));
        addGame_element(new Shield_Wielder(player_spawns[1],player_size,"Fighter 2", player_skins[1], 100, player_movement_stats[0], player_movement_stats[1], player_movement_stats[2], false, Qt::Key::Key_Left, Qt::Key::Key_Right, Qt::Key::Key_Up, *this));
        break;
    }
}

void Window::delAllGame_elements()
{
    for(Entity* elem : game_elements) { delete elem; }
    game_elements.clear();
}

QVector<Entity *> Window::getTo_be_added() const
{
    return to_be_added;
}

void Window::addTo_be_added(Entity *entity)
{
    to_be_added.append(entity);
}

void Window::delTo_be_added(Entity *entity)
{
    delete to_be_added.takeAt(game_elements.indexOf(entity));
}

QVector<Entity *> Window::getTo_be_deleted() const
{
    return to_be_deleted;
}

void Window::addTo_be_deleted(Entity *entity)
{
    to_be_deleted.append(entity);
}

void Window::delTo_be_deleted(Entity *entity)
{
    delete to_be_deleted.takeAt(game_elements.indexOf(entity));
}

int Window::getSelected_arena() const
{
    return selected_arena;
}

void Window::setSelected_arena(int value)
{
    selected_arena = value;
}
