#ifndef KICK_USER_H
#define KICK_USER_H

#include "character.h"
#include "kick.h"
#include <QElapsedTimer>

class Kick_User : public Character
{
public:
    // Constructors
    // initialisation par défaut de Character et Kick_User
    Kick_User(QPoint position, QSize size, Window &window);
    // initialisation des attributs d'une classe + initialisation par défaut de l'autre classe
    Kick_User(QPoint position, QSize size, double kick_range, double kick_damage, double kick_cooldown, double kick_duration, int kick_input, Window &window);
    Kick_User(QPoint position, QSize size, QString name, QString skin, double max_hp, double move_speed, double jump_speed, double gravity_speed, bool orientation, int move_left_input, int move_right_input, int jump_input, Window &window);
    // initialisation des attributs des 2 classes
    Kick_User(QPoint position, QSize size, QString name, QString skin, double max_hp, double move_speed, double jump_speed, double gravity_speed, bool orientation, int move_left_input, int move_right_input, int jump_input, double kick_range, double kick_damage, double kick_cooldown, double kick_duration, int kick_input, Window &window);
    // Destructor
    ~Kick_User();
    // Methods
    virtual void manage_inputs() override;
    // Getters and Setters
    double getKick_range() const;
    void setKick_range(double value);
    double getKick_damage() const;
    void setKick_damage(double value);
    double getKick_cooldown() const;
    void setKick_cooldown(double value);
    double getKick_duration() const;
    void setKick_duration(double value);
    int getKick_input() const;
    void setKick_input(int value);

private:
    // Attributes
    double kick_range;
    double kick_damage;
    double kick_cooldown;
    double kick_duration;
    int kick_input;
    QElapsedTimer kick_cooldown_delay;
    QElapsedTimer kick_duration_delay;
};

#endif // KICK_USER_H
