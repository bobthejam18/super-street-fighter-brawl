# Super Street Fighter Brawl
### Informations
- description : simple platformer and fighting game 
- type : school project (cancelled)
- date : 2020
---
### Usage
- prerequisites :
  - The use of the Windows OS
  - The use of a 64bits processor
- execution :
  - open the project folder in the file explorator
  - navigate to ```Code/build-Super_Street_Fighter_Brawl-Desktop_Qt_6_0_4_MSVC2019_64bit-Debug/debug```
  - execute the program via ```Super_Street_Fighter_Brawl.exe```
- controls :
  - left player :
    - jump -> Z
    - move left -> Q
    - move right -> D
    - cast projectile -> SPACE
  - right player :
    - jump -> UP
    - move left -> LEFT
    - move right -> RIGHT
    - use shield -> CTRL