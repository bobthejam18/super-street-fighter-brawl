#ifndef BACKGROUND_H
#define BACKGROUND_H

#include "entity.h"

class Background : public Entity
{
public:
    // Constructors
    Background(QPixmap image,Window &window);
    // Destructor
    ~Background();
    // Methods
    void update() override;
};

#endif // BACKGROUND_H
