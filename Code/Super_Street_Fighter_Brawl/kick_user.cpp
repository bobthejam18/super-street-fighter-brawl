#include "kick_user.h"
#include "game.h"

// Constructors
Kick_User::Kick_User(QPoint position, QSize size, Window &window)
: Character(position,size,window)
{
    qDebug() << "Creation d'un objet Kick User";
    this->kick_range = window.getGAME().size().width()*150/1920;
    this->kick_damage = 50;
    this->kick_cooldown = 1000;
    this->kick_duration = 250;
    this->kick_input = Qt::Key::Key_Space;
}

Kick_User::Kick_User(QPoint position, QSize size, double kick_range, double kick_damage, double kick_cooldown, double kick_duration, int kick_input, Window &window)
: Character(position,size,window)
{
    qDebug() << "Creation d'un objet Kick User";
    this->kick_range = kick_range;
    this->kick_damage = kick_damage;
    this->kick_cooldown = kick_cooldown;
    this->kick_duration = kick_duration;
    this->kick_input = kick_input;
}

Kick_User::Kick_User(QPoint position, QSize size, QString name, QString skin, double max_hp, double move_speed, double jump_speed, double gravity_speed, bool orientation, int move_left_input, int move_right_input, int jump_input, Window &window)
: Character(position,size,name,skin,max_hp,move_speed,jump_speed,gravity_speed,orientation,move_left_input,move_right_input,jump_input,window)

{
    qDebug() << "Creation d'un objet Kick User";
    qDebug() << "Creation d'un objet Kick User";
    this->kick_range = window.getGAME().size().width()*150/1920;
    this->kick_damage = 50;
    this->kick_cooldown = 1000;
    this->kick_duration = 250;
    this->kick_input = Qt::Key::Key_Space;
}

Kick_User::Kick_User(QPoint position, QSize size, QString name, QString skin, double max_hp, double move_speed, double jump_speed, double gravity_speed, bool orientation, int move_left_input, int move_right_input, int jump_input, double kick_range, double kick_damage, double kick_cooldown, double kick_duration, int kick_input, Window &window)
: Character(position,size,name,skin,max_hp,move_speed,jump_speed,gravity_speed,orientation,move_left_input,move_right_input,jump_input,window)
{
    qDebug() << "Creation d'un objet Kick User";
    this->kick_range = kick_range;
    this->kick_damage = kick_damage;
    this->kick_cooldown = kick_cooldown;
    this->kick_duration = kick_duration;
    this->kick_input = kick_input;
}

// Destructor
Kick_User::~Kick_User()
{
    qDebug() << "Suppression d'un objet Kick User";
}

// Methods
void Kick_User::manage_inputs()
{
    Character::manage_inputs();

    if(kick_duration_delay.elapsed()>=kick_duration) {
        for(Entity* entity : WINDOW.getGame_elements())
        {
            if(entity == this)
                continue;

            if(dynamic_cast<Kick*> (entity) != nullptr) {
                Kick * kick = dynamic_cast<Kick*> (entity);
                WINDOW.addTo_be_deleted(kick);
            }
        }
    }

    if(kick_cooldown_delay.elapsed()>=kick_cooldown) {
        if(WINDOW.getGAME().getKeys()[kick_input]) {
            kick_cooldown_delay.restart();
            kick_duration_delay.restart();
            QSize kick_size = QSize(kick_range,size.height()*3/4);
            QPoint kick_position = QPoint(orientation ? position.x()+size.width()/2 : position.x()+size.width()/2-kick_size.width(), position.y()+size.height()/2-kick_size.height()/2);
            WINDOW.addTo_be_added(new Kick(kick_position,kick_size,kick_damage,*this,WINDOW));
        }
    }
}

// Getters and Setters
double Kick_User::getKick_range() const
{
    return kick_range;
}

void Kick_User::setKick_range(double value)
{
    kick_range = value;
}

double Kick_User::getKick_damage() const
{
    return kick_damage;
}

void Kick_User::setKick_damage(double value)
{
    kick_damage = value;
}

double Kick_User::getKick_cooldown() const
{
    return kick_cooldown;
}

void Kick_User::setKick_cooldown(double value)
{
    kick_cooldown = value;
}

double Kick_User::getKick_duration() const
{
    return kick_duration;
}

void Kick_User::setKick_duration(double value)
{
    kick_duration = value;
}

int Kick_User::getKick_input() const
{
    return kick_input;
}

void Kick_User::setKick_input(int value)
{
    kick_input = value;
}
