#ifndef GAME_H
#define GAME_H

#include <QWidget>
#include <QKeyEvent>
#include <QPainter>
#include <QTimer>
#include <QElapsedTimer>
#include <QIcon>
#include <QtDebug>
#include "window.h"
#include "entity.h"
#include "character.h"
#include "platform.h"

class Game : public QWidget
{
    Q_OBJECT

public:
    // Constructors
    Game(QSize window_size = QSize(0,0));
    // Destructor
    ~Game();
    // Methods
    void paintEvent(QPaintEvent *event);
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);
    // Getters and Setters
    int getFPS() const;
    QMap<int, bool> const& getKeys() const;
    bool const& getGame_paused() const;
    void setGame_paused(bool value);
    bool const& getGame_over() const;
    void setGame_over(bool value);
    Window getWindow() const;

public slots:
    void update_game();

private:
    // Attributes
    const int FPS = 60;
    QTimer update_timer;
    QMap<int, bool> keys;
    bool game_paused;
    bool game_over;
    Window window;
};

#endif // GAME_H
