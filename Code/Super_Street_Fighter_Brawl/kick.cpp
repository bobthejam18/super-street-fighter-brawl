#include "kick.h"

// Constructors
Kick::Kick(QPoint position, QSize size, double damage, Character &kicker, Window & window)
: Entity(position,size,window), KICKER(kicker)
{
    qDebug() << "Creation d'un objet Kick";
    this->damage = damage;
}

Kick::Kick(QPoint position, QSize size, QPixmap image, double damage, Character &kicker, Window & window)
: Entity(position,size,image,window), KICKER(kicker)
{
    qDebug() << "Creation d'un objet Kick";
    this->damage = damage;
}

// Destructor
Kick::~Kick()
{
    qDebug() << "Suppression d'un objet Kick";
}

// Methods
void Kick::update()
{
    position = QPoint(KICKER.getOrientation() ? KICKER.getPosition().x()+KICKER.getSize().width()/2 : KICKER.getPosition().x()+size.width()/2-size.width(), KICKER.getPosition().y()+KICKER.getSize().height()/2-size.height()/2);
}

// Getters and Setters
Character &Kick::getKICKER() const
{
    return KICKER;
}

double Kick::getDamage() const
{
    return damage;
}

void Kick::setDamage(double value)
{
    damage = value;
}
