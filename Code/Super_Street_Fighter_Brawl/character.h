#ifndef CHARACTER_H
#define CHARACTER_H

#include <QVector2D>
#include <typeinfo>
#include <QtDebug>
#include "entity.h"

class Character : public Entity
{
public:
    // Constructors
    // initialisation par défaut de Character et Game_Entity
    Character(QPoint position, QSize size, Window &window);
    // initialisation des attributs des 2 classes
    Character(QPoint position, QSize size, QString name, QString skin, double max_hp, double move_speed, double jump_speed, double gravity_speed, bool orientation, int move_left_input, int move_right_input, int jump_input, Window &window);
    // Destructor
    ~Character();
    // Operators
    bool operator==(const Character& character) const;
    // Methods
    virtual void update() override;
    virtual void manage_inputs();
    virtual void manage_collisions();
    virtual void manage_animations();
    // Getters and Setters
    QString getName() const;
    void setName(const QString &value);
    QString getSkin() const;
    void setSkin(const QString &value);
    double getMax_hp() const;
    void setMax_hp(double value);
    double getHp() const;
    void setHp(double value);
    double getMove_speed() const;
    void setMove_speed(double value);
    double getJump_speed() const;
    void setJump_speed(double value);
    double getGravity_speed() const;
    void setGravity_speed(double value);
    QVector2D getVelocity() const;
    void setVelocity(const QVector2D &value);
    bool getIs_on_ground() const;
    void setIs_on_ground(bool value);
    bool getOrientation() const;
    void setOrientation(bool value);
    int getMove_left_input() const;
    void setMove_left_input(int value);
    int getMove_right_input() const;
    void setMove_right_input(int value);
    int getJump_input() const;
    void setJump_input(int value);

protected:
    // Attributes
    QString name;
    QString skin;
    double max_hp;
    double hp = max_hp;
    double move_speed;
    double jump_speed;
    double gravity_speed;
    QVector2D velocity;
    bool is_on_ground;
    bool orientation;
    int move_left_input;
    int move_right_input;
    int jump_input;
};

#endif // CHARACTER_H
