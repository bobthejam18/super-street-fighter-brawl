#include "text.h"

// Constructors
Text::Text(QPoint position, QSize size, QString text, Window & window) : Entity(position,size,text,window)
{
    qDebug() << "Creation d'un objet Text";
}

// Destructor
Text::~Text()
{
    qDebug() << "Suppression d'un objet Text";
}
