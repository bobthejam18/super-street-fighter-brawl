#include "projectile_caster.h"
#include "game.h"

// Constructors
Projectile_Caster::Projectile_Caster(QPoint position, QSize size, Window &window)
: Character(position,size,window)
{
    qDebug() << "Creation d'un objet Projectile Caster";
    this->projectile_speed = window.getGAME().size().width()*1850/1920;
    this->projectile_damage = 20;
    this->projectile_cooldown = 1000;
    this->projectile_input = Qt::Key::Key_Space;
}

Projectile_Caster::Projectile_Caster(QPoint position, QSize size, double projectile_speed, double projectile_damage, double projectile_cooldown, int projectile_input, Window &window)
: Character(position,size,window)
{
    qDebug() << "Creation d'un objet Projectile Caster";
    this->projectile_speed = projectile_speed;
    this->projectile_damage = projectile_damage;
    this->projectile_cooldown = projectile_cooldown;
    this->projectile_input = projectile_input;
}

Projectile_Caster::Projectile_Caster(QPoint position, QSize size, QString name, QString skin, double max_hp, double move_speed, double jump_speed, double gravity_speed, bool orientation, int move_left_input, int move_right_input, int jump_input, Window &window)
: Character(position,size,name,skin,max_hp,move_speed,jump_speed,gravity_speed,orientation,move_left_input,move_right_input,jump_input,window)
{
    qDebug() << "Creation d'un objet Projectile Caster";
    this->projectile_speed = window.getGAME().size().width()*1850/1920;
    this->projectile_damage = 20;
    this->projectile_cooldown = 1000;
    this->projectile_input = Qt::Key::Key_Space;
}

Projectile_Caster::Projectile_Caster(QPoint position, QSize size, QString name, QString skin, double max_hp, double move_speed, double jump_speed, double gravity_speed, bool orientation, int move_left_input, int move_right_input, int jump_input, double projectile_speed, double projectile_damage, double projectile_cooldown, int projectile_input, Window &window)
: Character(position,size,name,skin,max_hp,move_speed,jump_speed,gravity_speed,orientation,move_left_input,move_right_input,jump_input,window)
{
    qDebug() << "Creation d'un objet Projectile Caster";
    this->projectile_speed = projectile_speed;
    this->projectile_damage = projectile_damage;
    this->projectile_cooldown = projectile_cooldown;
    this->projectile_input = projectile_input;
}

// Destructor
Projectile_Caster::~Projectile_Caster()
{
    qDebug() << "Suppression d'un objet Projectile Caster";
}


// Methods
void Projectile_Caster::manage_inputs()
{
    Character::manage_inputs();

    if(cast_cooldown_delay.elapsed()>=projectile_cooldown) {
        if(WINDOW.getGAME().getKeys()[projectile_input]) {
            cast_cooldown_delay.restart();
            QSize projectile_size = QSize(double(WINDOW.getGAME().size().width())*(size.height()/2)/1920,double(WINDOW.getGAME().size().height())*(size.height()/4)/1080);
            QPoint projectile_position = QPoint(orientation ? position.x()+size.width()/2 : position.x()-size.width()/2, position.y()+size.height()/2-projectile_size.height()/2);
            WINDOW.addTo_be_added(new Projectile(projectile_position,projectile_size,projectile_damage,projectile_speed,orientation,*this,WINDOW));
        }
    }
}

// Getters and Setters
double Projectile_Caster::getProjectile_speed() const
{
    return projectile_speed;
}

void Projectile_Caster::setProjectile_speed(double value)
{
    projectile_speed = value;
}

double Projectile_Caster::getProjectile_damage() const
{
    return projectile_damage;
}

void Projectile_Caster::setProjectile_damage(double value)
{
    projectile_damage = value;
}

double Projectile_Caster::getProjectile_cooldown() const
{
    return projectile_cooldown;
}

void Projectile_Caster::setProjectile_cooldown(double value)
{
    projectile_cooldown = value;
}

int Projectile_Caster::getProjectile_input() const
{
    return projectile_input;
}

void Projectile_Caster::setProjectile_input(int value)
{
    projectile_input = value;
}
