#ifndef PROJECTILE_H
#define PROJECTILE_H

#include "entity.h"
#include "character.h"

class Projectile : public Entity
{
public:
    // Constructors
    Projectile(QPoint position, QSize size, double damage, double speed, bool direction, Character &caster, Window &window);
    Projectile(QPoint position, QSize size, QPixmap image, double damage, double speed, bool direction, Character &caster, Window &window);
    // Destructor
    ~Projectile();
    // Methods
    void update() override;
    void explode();
    // Getters and Setters
    Character &getCASTER() const;
    double getDamage() const;
    void setDamage(double value);
    double getSpeed() const;
    void setSpeed(double value);
    bool getDirection() const;
    void setDirection(bool value);

private:
    // Attributes
    Character &CASTER;
    double damage;
    double speed;
    bool direction;
};

#endif // PROJECTILE_H
