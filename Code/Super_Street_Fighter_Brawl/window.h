#ifndef WINDOW_H
#define WINDOW_H

#include <QPixmap>
#include <QVector>
#include <QtDebug>
#include "entity.h"
#include "character.h"
#include "projectile_caster.h"
#include "kick_user.h"
#include "shield_wielder.h"
#include "platform.h"
#include "background.h"

class Game;

class Window
{
public:
    // Constructors
    Window(Game &game);
    // Destructor
    ~Window();
    // Methods
    void init_game();
    void update_game();
    void repaint_game(QPainter &painter);
    // Getters and Setters
    Game &getGAME() const;
    QVector<Entity *> getGame_elements() const;
    void addGame_element(Entity * entity);
    void delGame_element(Entity * entity);
    void addAllGame_elements();
    void delAllGame_elements();
    QVector<Entity *> getTo_be_added() const;
    void addTo_be_added(Entity * entity);
    void delTo_be_added(Entity * entity);
    QVector<Entity *> getTo_be_deleted() const;
    void addTo_be_deleted(Entity * entity);
    void delTo_be_deleted(Entity * entity);
    int getSelected_arena() const;
    void setSelected_arena(int value);

private:
    // Attributes
    Game &GAME;
    QVector<Entity *> game_elements;
    QVector<Entity *> to_be_added;
    QVector<Entity *> to_be_deleted;
    int selected_arena;
};

#endif // WINDOW_H
